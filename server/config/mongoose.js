var config = require('./config'),
	mongoose = require('mongoose');

module.exports = function() {
	var db = mongoose.connect(config.db);

	require('../models/user.model');
	require('../models/alias.model');
	require('../models/region.model');
    require('../models/address.model');
    require('../models/incognito.model');

	return db;
};