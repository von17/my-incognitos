var config = require('./config'),
    express = require('express'),
    morgan = require('morgan'),
    compress = require('compression'),
    bodyParser = require('body-parser'),
    methodOverride = require('method-override'),
    session = require('express-session'),
    MongoStore = require('connect-mongo')(session),
    flash = require('connect-flash'),
    passport = require('passport'),
    fs = require('fs'),
    https = require('https');

module.exports = function(db) {
    var app = express();
    var server = https.createServer({
        key: fs.readFileSync('./server/config/certs/mi.key'),
        cert: fs.readFileSync('./server/config/certs/mi.crt'),
        passphrase: 'Sp00nS2000'
    }, app);

    if (process.env.NODE_ENV === 'development') {
        app.use(morgan('dev'));
    } else if (process.env.NODE_ENV === 'production') {
        app.use(compress());
    }

    app.use(bodyParser.urlencoded());
    app.use(bodyParser.json());
    app.use(methodOverride());

    var mongoStore = new MongoStore({
        mongooseConnection: db.connection
    });

    /* CHANGE THIS!!! */
    app.use(session({
        saveUninitialized: true,
        resave: true,
        secret: config.sessionSecret,
        store: mongoStore
    }));

    app.use(flash());
    app.use(passport.initialize());
    app.use(passport.session());

    app.use(express.static('./web/'));

    app.route('/*')
        .get(function(req, res) {
            res.sendfile('./web/index.html');
        });

    return server;
};