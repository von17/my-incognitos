var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var AliasSchema = new Schema({
	firstName: {
		type: String,
		trim: true,
        lowercase: true,
		required: 'First name is required',
		minlength: [1, 'First name must contain at least {MINLENGTH} character'],
		maxlength: [30, 'First name cannot exceed {MAXLENGTH} characters'],
		match: [/[a-z]+[\-\'\.\s]?[a-z]*/i, 'Invalid first name']
	},
	lastName: {
		type: String,
		trim: true,
        lowercase: true,
		required: 'Last name is required',
		minlength: [1, 'Last name must contain at least {MINLENGTH} character'],
		maxlength: [30, 'Last name cannot exceed {MAXLENGTH} characters'],
		match: [/[a-z]+[\-\'\.\s]?[a-z]*/i, 'Invalid last name']
	},
    gender: {
        type: String,
        required: 'Gender is required',
        enum: ['m', 'f', 'g', 'l', 'b', 't']
    },
    creator: {
        type: Schema.ObjectId,
        ref: 'User',
        required: 'Creator is required'
    },
	timesUsed: {
		type: Number,
        min: [0, 'Times used cannot be less than {MIN}'],
		default: 0
	}
});

mongoose.model('Alias', AliasSchema);