var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var AddressSchema = new Schema({
    address: {
        type: String,
        lowercase: true,
        trim: true,
        required: 'Address is required',
        match: [/.+/, 'Address should contain at least 1 character'],
        maxlength: [50, 'Address cannot exceed {MAXLENGTH} characters']
    },
    region: {
        type: Schema.ObjectId,
        ref: 'Region',
        required: 'Region is required'
    }
});

mongoose.model('Address', AddressSchema);