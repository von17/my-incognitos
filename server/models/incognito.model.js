var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var IncognitoSchema = new Schema({
    alias: {
       type: Schema.ObjectId,
       ref: 'Alias',
       required: 'Alias is required'
    },
    address: {
        type: Schema.ObjectId,
        ref: 'Address'
    },
    emails: [String],
    usedIn: [String],
    traits: [String],
    comments: {
        type: String,
        trim: true
    }
});

mongoose.model('Incognito', IncognitoSchema);