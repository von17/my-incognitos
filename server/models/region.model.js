var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var RegionSchema = new Schema({
	city: {
		type: String,
		lowercase: true,
        trim: true,
		required: 'City/Town is required',
		minlength: [1, 'City/Town must contain at least {MINLENGTH} character'],
		maxlength: [30, 'City/Town cannot exceed {MAXLENGTH} characters'],
		match: [/[\w\d]+[\-\'\.\s]?[\w\d]*/i, 'Invalid City/Town']
	},
	country: {
		type: String,
		lowercase: true,
        trim: true,
		required: 'Country is required',
        minlength: [1, 'Country must contain at least {MINLENGTH} character'],
		maxlength: [30, 'Country cannot exceed {MAXLENGTH} characters'],
		match: [/[\w\d]+[\-\'\.\s]?[\w\d]*/i, 'Invalid Country']
	},
	postalCode: {
		type: String,
		lowercase: true,
        trim: true,
        minlength: [1, 'Postal Code must contain at least {MINLENGTH} character'],
		maxlength: [20, 'Postal Code cannot exceed {MAXLENGTH} characters'],
		match: [/[\w\d]+[\-\'\.\s]?[\w\d]*/i, 'Invalid Postal Code']
	}
});

mongoose.model('Region', RegionSchema);