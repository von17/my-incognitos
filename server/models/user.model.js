var mongoose = require('mongoose'),
	crypto = require('crypto'),
	Schema = mongoose.Schema;

var UserSchema = new Schema({
	email: {
		type: String,
		trim: true,
		unique: 'Email address already exists',
		required: 'Email address is required',
		match: [/.+\@.+\..+/, 'Invalid email address'],
		maxlength: [50, 'Email cannot exceed {MAXLENGTH} characters']
	},
	password: {
		type: String,
		required: 'Password is required',
		validate: [
			function(password) {
				return password && password.length >= 8 && password.length <= 16;
			}, 'Passwords should be between 8 and 16 characters'
		],
		match: [/.*[0-9]+.*/, 'Passwords should contain at least 1 number']
	},
	salt: {
		type: String
	},
	provider: {
		type: String,
		required: 'Provider is required'
	},
	providerId: String,
	providerData: {},
	created: {
		type: Date,
		default: Date.now
	}
});

UserSchema.pre('save', function(next) {
	if (this.password) {
		this.salt = new
			Buffer(crypto.randomBytes(16).toString('base64'), 'base64');
		this.password = this.hashPassword(this.password);
	}
	
	next();
});

UserSchema.methods.hashPassword = function(password) {
	return crypto.pbkdf2Sync(password, this.salt, 10000, 64).toString('base64');
};

UserSchema.methods.authenticate = function(password) {
	return this.password === this.hashPassword(password);
};

mongoose.model('User', UserSchema);