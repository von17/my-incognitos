exports.getErrMsg = function(duplicateMsg, err) {
    var defaultMsg = 'Something went wrong';

    if (err.code) {
        switch (err.code) {
            case 11000:
            case 11001:
                return duplicateMsg;
            default:
                return defaultMsg;
        }
    } else {
        for (var errName in err.errors) {
            if (err.errors[errName].message) {
                return err.errors[errName].message;
            } else {
                return defaultMsg;
            }
        }
    }

    return msg;
};