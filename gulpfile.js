var gulp = require('gulp'),
    rimraf = require('gulp-rimraf'),
    concat = require('gulp-concat'),
    ngdocs = require('gulp-ngdocs'),
    sass = require('gulp-sass'),
    uglify = require('gulp-uglify'),
    runSequence = require('run-sequence');

var allJS = ['./web/res/bower/modernizr-min/modernizr.min.js', './web/res/bower/angular/angular.min.js',
				'./web/res/bower/angular-ui-router/release/angular-ui-router.min.js', './web/res/bower/jquery/dist/jquery.min.js',
                './web/res/bower/bootstrap/dist/js/bootstrap.min.js',
                './web/res/js/app.js', './web/res/js/modules.js', './web/res/js/configs.js'];

gulp.task('concatAllJS', function() {
    return gulp.src(allJS)
        .pipe(concat('all.js'))
        .pipe(gulp.dest('./web/res/'));
});

gulp.task('concatAllCSS', function() {
    return gulp.src(['./web/res/bower/bootstrap/dist/css/bootstrap.min.css'])
        .pipe(concat('all.css'))
        .pipe(gulp.dest('./web/res/'));
});

gulp.task('buildStyles', function(callback) {
    runSequence('concatAllCSS', callback);
});

gulp.task('bootstrapFonts', function() {
    return gulp.src('./web/res/bower/bootstrap/dist/fonts/*')
        .pipe(gulp.dest('./web/fonts/'));
});

gulp.task('default', function(callback) {
    runSequence('concatAllJS', 'buildStyles', 'bootstrapFonts', 'watch', callback);
});

gulp.task('watch', function() {
    gulp.watch('./web/res/js/**/*.js', ['concatAllJS']);
    // watch scss files
});