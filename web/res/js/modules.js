(function(Modules, undefined) {
    /**
     * @ngdoc object
     * @name MI Modules
     * @id mi_modules
     * @description
     *
     * This object sets up the angular modules.
    **/
    Modules.Home = angular.module('Home', []);
    Modules.MI = angular.module('MI', []);
    Modules.News = angular.module('News', []);
    Modules.About = angular.module('About', []);
    Modules.LogIn = angular.module('LogIn', []);

    Modules.MainApp = angular.module('MainApp', ['Home', 'MI', 'News', 'About', 'LogIn', 'ui.router']);
}(MI.Modules));
