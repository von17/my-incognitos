(function(Modules, Configs, Path, undefined) {
    Modules.MainApp.config(['$stateProvider', '$urlRouterProvider', '$locationProvider',
        function($stateProvider, $urlRouterProvider, $locationProvider) {
            $locationProvider.hashPrefix('!');

            $urlRouterProvider.otherwise('/');

            $stateProvider
                .state('home', {
                    url: '/',
                    templateUrl: Path + '/home.html'
                })
                .state('myIncognitos', {
                    url: '/myIncognitos',
                    templateUrl: Path + '/myIncognitos.html'
                })
                .state('news', {
                    url: '/news',
                    templateUrl: Path + '/news.html'
                })
                .state('about', {
                    url: '/about',
                    templateUrl: Path + '/about.html'
                })
                .state('logIn', {
                    url: '/logIn',
                    templateUrl: Path + '/logIn.html'
                });
        }
    ]);
}(MI.Modules, MI.Configs, MI.TemplatesPath));
