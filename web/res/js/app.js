(function(MI, undefined) {
    /**
     * @ngdoc function
     * @name MI
     * @id mi
     * @description
     *
     * Set up MI parameters.
    **/
    MI.Version = '1.0.0';
    MI.TemplatesPath = './res/templates';
    MI.Services = {};
    MI.Factories = {};
    MI.Modules = {};
    MI.Configs = {};
    MI.Controllers = {};
    MI.Directives = {};
}(window.MI = window.MI || {}));
