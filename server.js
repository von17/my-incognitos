process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var express = require('./server/config/express'),
    mongoose = require('./server/config/mongoose');

var db = mongoose();
var app = express(db);

app.listen(3000);

module.exports = app;

console.log('Server running at https://localhost:3000/');